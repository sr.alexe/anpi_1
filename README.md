# ANPI_1

In the p1_metodos.py there is an attempt to implement the Regula falsi, bisection and secant methods for solving continuos equations where F(x)=0  and x is unknown. And in p1_metodo_nuevo.py is an implementation of an improved Newtons-Rapson method by Muhammad Aslam Noor.
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
To run the code just run the files with your python 3.x interpreter 

```bash
python p1_metodos.py
```

or

```bash
python p1_metodo_nuevo.py
```

---

### Prerequisites

To run this code you need the following dependencies:

`Sympy`

`matplotlib`

`numpy`

`scipy`

`plotly`

### To avoid the bad configured environments and unused dependencies is recommended to install Anaconda and create a new enviroment firts this can be achived with the following command

```bash
conda create --name *myenv*
```

Then enable the new enviroment

```bash
conda activate *myenv*
```

---

### Installing

There is no installation needed

## Deployment

This code is unoptimized it should not be implemented in other projects before understanding its behavior

## Built With

* [anaconda](https://www.anaconda.com/) - The enviroments manager
* [Python](https://www.python.org/) - Programming language
* [Plotly](https://plotly.com/) - Used to generate charts
* [numpy](https://numpy.org/) - Used to run complex arrays
* [Sympy](https://www.sympy.org/en/index.html) - Used to create symbolic equations

## Versioning

We use [GitLab](https://gitlab.com/) for versioning. For the versions available, see the [repository](https://gitlab.com/sr.alexe/anpi_1).

## Authors

* **Alex Saenz Rojas** - *Initial work* - [sr.alexe](https://gitlab.com/sr.alexe/)
* **Daniel Sanabria** - *Math tests* - [djsa94](https://gitlab.com/djsa94)
* **Gerald Mora** - *Optimization* - [geraldparkm1](https://gitlab.com/geraldparkm1)

See also the list of [contributors](https://gitlab.com/sr.alexe/anpi_1/-/project_members) who participated in this project.

## License

This project is licensed under the GPL License - see the [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) file for details

## Acknowledgments

* Thanks to my mom and GF fo all the coffee and patience
