from sympy import *
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate
from plotly.subplots import make_subplots
import plotly.graph_objects as go


x, x1, d, x2, alpha, k, sigmaR, sigmaC, lambda0, r, S, sigmadB = symbols(
    'x x1 d x2 alpha k sigmaR sigmaC lambda0 r S sigmadB')
g = Function('g')(d)  # Initialize the g(d) function from the original function
H = Function('H')(d)  # Initialize the F(d) function to find the distance
W = Function('W')(d)  # Parametric equations for the function
F = Function('F')(d)  # Equation to calculate new  X value
r, alpha, sigmadB, lambda0, x1, x2 = 10, 4, 4, 1, 7, 6  # Initialize parameters

chart_iterations = []
chart_errors = []
chart_xs = []


S = pi * (r ** 2)
sigmaR = sqrt((sigmadB ** 2) / (10 * alpha) ** 2) # Calculates σR parameter
k = (10 * alpha) / (ln(10))
g = ((2 * S) / (pi)) * acos((d) / (2 * r)) - \
    d * sqrt((r ** 2) - ((d ** 2) / (4))) ### g(d) sympy function
sigmaC = sqrt(((g ** 2) / (2 * lambda0 * k ** 2))
              * (((1) / (g)) + ((1) / (S)))) # Initialization of the σc parameter
H = ((log(x1 / d, 10)) / ((sigmaR ** 2) * ln(10))) + \
    ((d * (x2 - d)) / (sigmaC ** 2)) # Initialization of the main function F(d)
W = ((F) * (F.diff(d).diff(d))) / ((F.diff(d)) ** 2) # Initialization of the parametric function  W
xk1 = d - (1 + ((1 * W) / 2) + ((W ** 2) / (2) +
                                ((W ** 3) / (4)))) * ((F) / (F.diff(d))) ##Calculates next X


def Muhammad_Noor_method(a, H):
    """[Calculates new X with a one point formula]

    Parameters
    ----------
    a : [int]
        [Starting point in X axis]
    H : [Sympy]
        [Function to evaluate ]

    Returns
    -------
    [Tuple]
        [Contains 3 arrays (Iterations, errors and ds)]
    """
    W = ((F) * (F.diff(d).diff(d))) / ((F.diff(d)) ** 2)
    xk1 = d - (1 + ((1 * W) / 2) + ((W ** 2) / (2) +
                                    ((W ** 3) / (4)))) * ((F) / (F.diff(d)))
    x = xk1.subs(F, H).subs(d, a).doit().evalf()
    ek = abs(H.subs(d, x).evalf())
    errors = []
    iterations = []
    xs = []
    k = 0
    while (ek > 10 ** -10):
        x = xk1.subs(F, H).subs(d, a).doit().evalf() ## Calculate next X
        ek = abs(H.subs(d, x).evalf()) ## Calculate error ek
        a = x #substitutes a to X for next iteration
        errors.append(ek)##Adding results to arrays
        iterations.append(k)
        xs.append(x)
        k += 1

    return iterations, errors, xs

def Plot_Arrays(x_Axis, y_Axis1, y_Axis2):
    """[Plot arrays with plotly, using subplots, adds Rows depending on the length of iterations]

    Parameters
    ----------
    x_Axis : [Array]
        [Number of pointsin the X axis]
    y_Axis1 : [Array]
        [Array of points of values in Y for column 1]
    y_Axis2 : [Array]
        [Array of points of values in Y for column 2]
    """
    fig = make_subplots(rows=len(x_Axis), cols=2, horizontal_spacing=0.06, subplot_titles=("|F(x)| Muhammad Noor M  ethod",
                                                                                           "X Muhammad Noor Method",)) ## Name of each subplot
    for i in range(len(x_Axis)):
        float_iterations = np.float64(x_Axis[i]) ## Transforms arrays into numpy array
        float_errors = np.float64(y_Axis1[i])
        float_ds = np.float64(y_Axis2[i])
        fig.add_trace( ## Adds subplot to row and col 1
            go.Scatter(x=float_iterations, y=float_errors,
                       line_shape='spline'),
            row=i+1, col=1
        )
        fig.add_trace( ## Adds subplot to row and col 2
            go.Scatter(x=float_iterations, y=float_ds, line_shape='spline'),
            row=i+1, col=2
        )
        fig.update_xaxes(title_text="Iteration", row=i + 1, col=1)  ## Adds the axis titles
        fig.update_xaxes(title_text="Iteration", row=i + 1, col=2)
        fig.update_yaxes(title_text="|F(x)|", row=i + 1, col=1)
        fig.update_yaxes(title_text="x", row=i + 1, col=2,)
    fig.update_layout(
        title={
            'text': "F(x) and x vs Iterations ",## Adds chart title
            'x': 0.5
        },
        font=dict(
            family="Courier New, monospace",
            size=18,
            color="#7f7f7f"
        ))
    fig.show()
    return


iterations, errors, xs = Muhammad_Noor_method(5, H) ## Start Function H with start point 3
chart_iterations.append(iterations)
chart_errors.append(errors)
chart_xs.append(xs)

Plot_Arrays(chart_iterations, chart_errors, chart_xs)


# 14.6680104210341
    