from sympy import *
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate
from plotly.subplots import make_subplots
import plotly.graph_objects as go
evaluate(False)  # Disables Auto simplification of
chart_iterations = []
chart_errors = []
chart_ds = []
d, k, sigmaR, sigmaC, S,  = symbols(
    'd k sigmaR sigmaC S')  # Generates Sympy symbols necessary to define the functions
r, alpha, sigmadB, lambda0, x1, x2 = 10, 4, 4, 1, 7, 6  # Initialize parameters
g = Function('g')(d)  # Initialize the g(d) function from the original function
H = Function('H')(d)  # Initialize the F(d) function to find the distance
S = pi*r**2  # Calculates the S parameter
g = (2*S/pi)*acos(d/(2*r))-d*sqrt(r**2-(d**2/4))  # g(d) sympy function
k = (10*alpha)/ln(10)  # Calculates K parameter
sigmaR = sqrt((sigmadB**2)/(10*alpha)**2)  # Calculates σR parameter
# Initialization of the σc parameter
sigmaC = sqrt(((g**2)/(2*lambda0*k**2))*((1/g)+(1/S)))
H = (log(x1 / d, 10) / ((sigmaR ** 2) * ln(10))) + \
    (d * (x2 - d)) / (sigmaC ** 2)  # Initialization of the main function F(d)


def regula_falsi(a, b, F):
    """[Implementation of the regula falsi method by utilizing symbolic python.
        Creates 3 arrays to store the number of iterations (iterations), the distance calculations (ds)]
    Parameters
    ----------
    a : [int]
        [Initial Value to start the approximation method to start interval ]
    b : [int]
        [Final Value of the initial interval]
    F : [Sympy Function]
        [The function which the zero will be approximated]
    Returns
    -------
    [Tuple]
        [Contains 3 arrays (Iterations, errors and ds)]
    """
    errors = []  # Array of absolute errors calculated by |F(x)|
    ds = []  # Array of approximations of d
    iterations = []  # Array with the numbers of iterations
    xk, xk_1 = symbols("xk xk_1 ")  # Initialization of Xk and Xk+1 symbols
    k = 0  # Iterator
    xks = xk - (((xk - xk_1) / (F.subs(d, xk) - F.subs(d, xk_1)))
                * F.subs(d, xk))
    fa = F.subs(d, a).evalf()  # F(a) value for Bolzano
    fb = F.subs(d, b).evalf()  # F(b) value for Bolzano
    if (not (fa * fb <= 0)):
        print("Doesn't satisfy Bolzano condition")
    # Calculation of new X2 value based on previous values
    c = xks.subs(xk, b).subs(xk_1, a).evalf()
    ek = abs(F.subs(d, c).evalf())  # Error calculated by |F(x)|
    while (ek > 10 ** -10):  # Condition of stop based on error calculation
        c = xks.subs(xk, b).subs(xk_1, a).evalf()  # New C for next iteration
        fc = F.subs(d, c).evalf()  # F(c) to evaluate Bolzano
        if(fa*fc < 0):  # If F(a)F(c)<0 then [a,c]
            a = a
            b = c
        elif (fb*fc < 0):  # If F(b)F(c)<0 then [c,b]
            a = c
            b = b
        elif (fc == 0):
            break
        errors.append(ek)  # Adding results to arrays
        ds.append(c)
        iterations.append(k)
        ek = abs(F.subs(d, c).evalf())
        k += 1
    return iterations, errors, ds  # Returns 3 arrays


def secant(a, b, F):
    """[Implementation of the secant method by utilizing symbolic python.
        Creates 3 arrays to store the number of iterations (iterations), the absolute error (errors) and the distance (ds)]
    Parameters
    ----------
    a : [int]
        [Initial Value to start the approximation method to start interval ]
    b : [int]
        [Final Value of the initial interval]
    F : [Sympy Function]
        [The function which the zero will be approximated]
    Returns
    -------
    [Tuple]
        [Contains 3 arrays (Iterations, errors and ds)]
    """
    errors = []  # Array of absolute errors calculated by |F(x)|
    ds = []  # Array of approximations of d
    iterations = []  # Array with the numbers of iterations
    xk, xk_1 = symbols("xk xk_1 ")  # Initialization of Xk and Xk+1 symbols
    k = 0  # Iterator
    xks = xk-((xk-xk_1)/(F.subs(d, xk)-F.subs(d, xk_1)))*F.subs(d, xk)
    # X calculated using the interval [a,b]
    xtemp = xks.subs(xk, b).subs(xk_1, a).evalf()
    ek = abs(F.subs(d, xtemp).evalf())  # Error calculated by |F(x)|
    while (ek > 10 ** -10):  # Condition of stop based on error calculation
        xtemp = xks.subs(xk, b).subs(xk_1, a).evalf()
        a = b  # Swaps a to b
        b = xtemp  # Changing b to the caluclated X value create the new interval
        # New X calculated with previous results
        xtemp = xks.subs(xk, b).subs(xk_1, a).evalf()
        ek = abs(F.subs(d, xtemp).evalf())
        errors.append(ek)  # Adding results to arrays
        ds.append(xtemp)
        iterations.append(k)
        k += 1
    return iterations, errors, ds  # Returns 3 arrays  as tuple


def bisection(a, b, F):
    """[Implementation of the bisection method by utilizing symbolic python.
        Creates 3 arrays to store the number of iterations (iterations), the absolute error (errors) and the distance (ds)]
    Parameters
    ----------
    a : [int]
        [Initial Value to start the approximation method to start interval ]
    b : [int]
        [Final Value of the initial interval]
    F : [Sympy Function]
        [The function which the zero will be approximated]
    Returns
    -------
    [Tuple]
        [Contains 3 arrays (Iterations, errors and ds)]
    """
    errors = []  # Array of absolute errors calculated by |F(x)|
    ds = []  # Array of approximations of d
    iterations = []  # Array with the numbers of iterations
    x = (a+b)/2 ## Calculate half point
    k = 0  # Iterator
    ek2 = abs(F.subs(d, x).evalf())
    while (ek2 > 10 ** -10):
        x = (a+b)/(2)
        fa = F.subs(d, a).evalf() # F(a) value for Bolzano
        fb = F.subs(d, b).evalf() # F(b) value for Bolzano
        if (fa*fb <= 0):
            fx = F.subs(d, x).evalf()
            ek2 = abs(F.subs(d, x).evalf())
            if(fa*fx <= 0): # If F(a)F(c)<0 then [a,x]
                b = x
            elif (fb*fx < 0): # If F(b)F(c)<0 then [x,b]
                a = x
        else:
            print("Doesn't satisfy Bolzano condition")
        errors.append(ek2)# Adding results to arrays
        ds.append(x)
        iterations.append(k)
        k += 1
    return iterations, errors, ds # Returns 3 arrays


def Plot_Arrays(x_Axis, y_Axis1, y_Axis2):
    """[Plot arrays with plotly, using subplots, adds Rows depending on the length of iterations]
    method
    x_Axis : [Array]
        [Number of pointsin the X axis]
    y_Axis1 : [Array]
        [Array of points of values in Y for column 1]
    y_Axis2 : [Array]
        [Array of points of values in Y for column 2]
    """
    fig = make_subplots(rows=len(x_Axis), cols=2, horizontal_spacing=0.06, subplot_titles=("|F(x)| Bisection Method",
                                                                                           "X Bisection Method", "|F(x)| Secant Method",
                                                                                           "X Secant Method", "|F(x)| Regula falsi Method",
                                                                                           "X Regula falsi Method",))  ## Name of each subplot
    for i in range(len(x_Axis)):
        float_iterations = np.float64(x_Axis[i]) ## Transforms arrays into numpy array
        float_errors = np.float64(y_Axis1[i])
        float_ds = np.float64(y_Axis2[i])
        fig.add_trace( ## Adds subplot to row and col 1
            go.Scatter(x=float_iterations, y=float_errors,
                       line_shape='spline'),
            row=i+1, col=1
        )
        fig.add_trace( ## Adds subplot to row and col 2
            go.Scatter(x=float_iterations, y=float_ds, line_shape='spline'),
            row=i+1, col=2
        )
        fig.update_xaxes(title_text="Iteration", row=i + 1, col=1)  ## Adds the axis titles
        fig.update_xaxes(title_text="Iteration", row=i + 1, col=2)
        fig.update_yaxes(title_text="|F(x)|", row=i + 1, col=1)
        fig.update_yaxes(title_text="x", row=i + 1, col=2,)
    fig.update_layout(
        title={
            'text': "F(x) and x vs Iterations ",## Adds chart title
            'x': 0.5
        },
        font=dict(
            family="Courier New, monospace",
            size=18,
            color="#7f7f7f"
        ))
    fig.show()
    return


iterations, errors, ds = bisection(5, 7, H) ## runs bisection
chart_iterations.append(iterations)
chart_errors.append(errors)
chart_ds.append(ds)
iterations, errors, ds = secant(5, 7, H)## runs secant
chart_iterations.append(iterations)
chart_errors.append(errors)
chart_ds.append(ds)
iterations, errors, ds = regula_falsi(5, 7, H)## runs regula falsi
chart_iterations.append(iterations)
chart_errors.append(errors)
chart_ds.append(ds)
Plot_Arrays(chart_iterations, chart_errors, chart_ds)  ##plots the results

it = []
result = []
for i in np.arange(0, 8, 0.1): ##plots the function
    if (not (i == 0)):
        result.append(H.subs(d, i).evalf())
        it.append(i)
np_it = np.float64(it)
np_result = np.float64(result)
fig = go.Figure(data=go.Scatter(x=np_it, y=np_result))
fig.show()
